/**
 * @title Node Template
 * @description Basic overhead for a website geared towards performance.
 * @author ethancirst
 **/
 
// [DEPENDENCIES]
const micro = require('iomicro')
const fs = require('fs') 
const title = 'Node Template'
const cdn = '/res/'
const cache = new Date().getTime()

// Serving '/res' files.
micro.get('/res/*', (req, res) => { 
    const resource = __dirname + "/views/res/" + req.params[0]

    fs.exists(resource, (exists) => {
        exists ? res.sendFile(resource) : res.status(404).render('404') 
    })
});

micro.get('/', (req, res) => {
	// NOTE ON CACHING:
	// The cache variable is a Unix timestamp when the server starts up.
	// This is appended to every file/resource loaded on the site, like: <fileName>?_=<cache>
	// This ensures that whenever a change is published and the server is restarted, all browsers will update their cache.
    res.render('home', { title: title, cdn: cdn, cache: cache })
})

micro.use('*', (req, res) => {
    res.status(404).render('404') 
})

micro.listen(3000, {
    appName: title,
    ssl: {
        key: '/etc/letsencrypt/live/narrownode.org/privkey.pem',
        cert: '/etc/letsencrypt/live/narrownode.org/fullchain.pem'
    }
})
